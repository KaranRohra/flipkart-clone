import os

from accounts import models
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = models.User(**validated_data, is_active=True)
        user.set_password(user.password)

        user.is_superuser = os.environ.get("IS_SUPERUSER") == "True"
        user.is_staff = os.environ.get("IS_SUPERUSER") == "True"

        user.save()
        return user

    class Meta:
        model = models.User
        fields = ("id", "email", "password", "first_name", "last_name")
