from accounts import apis
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

app_name = "accounts"


urlpatterns = [
    path("login", obtain_auth_token, name="auth"),
    path("user", apis.UserDetailsApi.as_view(), name="user-details"),
    path("", apis.UserApi.as_view(), name="create-user"),
]
