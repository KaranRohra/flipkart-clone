from accounts import models
from accounts import serializers
from rest_framework import generics
from rest_framework import permissions
from rest_framework import views
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response


class UserApi(generics.CreateAPIView):
    serializer_class = serializers.UserSerializer
    queryset = models.User.objects.all()


class UserDetailsApi(views.APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return Response(serializers.UserSerializer(request.user).data)
