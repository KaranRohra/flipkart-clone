from django.contrib import admin
from products import models


class ProductImageAdmin(admin.TabularInline):
    model = models.ProductImage
    extra = 3


class DiscountInline(admin.StackedInline):
    model = models.Discount
    extra = 3


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ("id", "title")
    inlines = (ProductImageAdmin, DiscountInline)
    search_fields = ("title", "short_title")
    list_filter = ("category",)


class SpecificationInline(admin.StackedInline):
    model = models.Specification
    extra = 3


@admin.register(models.SpecificationTitle)
class SpecificationTitleAdmin(admin.ModelAdmin):
    list_display = ("id", "title")
    search_fields = ("title",)
    inlines = (SpecificationInline,)
