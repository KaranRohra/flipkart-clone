from products import models
from products import serializers
from rest_framework import generics
from rest_framework import views
from rest_framework.response import Response


class ProductApi(generics.RetrieveAPIView):
    serializer_class = serializers.ProductSerializer
    queryset = models.Product.objects.all()


class ProductShortDeatilApi(views.APIView):
    def get(self, request, *args, **kwargs):
        products = models.Product.objects.filter(id__range=(1, 16))
        response = [
            {
                "id": product.id,
                "image_url": request.build_absolute_uri(product.images.first().image_url).replace("products", "media"),
                "title": product.short_title,
                "discount_title": product.discounts.first().short_title,
                "slogan": product.slogan,
            }
            for product in products
        ]
        return Response(response)
