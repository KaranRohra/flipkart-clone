from products import models
from rest_framework import serializers


class SpecificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Specification
        fields = ("name", "value")


class SpecificationTitleSerializer(serializers.ModelSerializer):
    specifications = SpecificationSerializer(many=True, read_only=True)

    class Meta:
        model = models.SpecificationTitle
        fields = ("title", "specifications")


class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductImage
        fields = ("image_url",)


class DiscountSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Discount
        fields = ("title", "short_title", "discount_percentage")


class ProductSerializer(serializers.ModelSerializer):
    images = ProductImageSerializer(many=True, read_only=True)
    specification_titles = SpecificationTitleSerializer(many=True, read_only=True)
    discounts = DiscountSerializer(many=True, read_only=True)

    class Meta:
        model = models.Product
        fields = "__all__"
