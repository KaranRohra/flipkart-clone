from django.urls import path
from products import apis

app_name = "products"

urlpatterns = [
    path("<int:pk>", apis.ProductApi.as_view(), name="product"),
    path("home", apis.ProductShortDeatilApi.as_view(), name="home-product"),
]
