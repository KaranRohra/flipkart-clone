CATEGORY = (
    ("Mobile", "Mobile"),
    ("TV", "Television"),
    ("Laptop", "Laptop"),
    ("GP", "Gaming Laptop"),
    ("Camera", "Camera"),
    ("Speaker", "Speaker"),
    ("AR", "Air Conditioner"),
    ("WS", "Washing Machine"),
    ("Other", "Other"),
)
