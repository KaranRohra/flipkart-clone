# Generated by Django 3.2.5 on 2021-08-03 06:24
import django.db.models.deletion
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Specification",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("name", models.CharField(help_text="Example brand, weight, etc", max_length=50)),
                ("value", models.CharField(help_text="Corresponding value of name", max_length=150)),
            ],
        ),
        migrations.CreateModel(
            name="SpecificationTitle",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("title", models.CharField(max_length=250)),
                ("created_date", models.DateTimeField(auto_now_add=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
            ],
            options={
                "verbose_name": "Specification",
                "verbose_name_plural": "Specifications",
            },
        ),
        migrations.RemoveField(
            model_name="descriptiontitle",
            name="product",
        ),
        migrations.RenameField(
            model_name="product",
            old_name="price",
            new_name="selling_price",
        ),
        migrations.AddField(
            model_name="product",
            name="description",
            field=models.TextField(default="Product Description"),
        ),
        migrations.AddField(
            model_name="product",
            name="original_price",
            field=models.PositiveIntegerField(default=0, verbose_name="MRP"),
        ),
        migrations.DeleteModel(
            name="Description",
        ),
        migrations.DeleteModel(
            name="DescriptionTitle",
        ),
        migrations.AddField(
            model_name="specificationtitle",
            name="product",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, related_name="specification_titles", to="products.product"
            ),
        ),
        migrations.AddField(
            model_name="specification",
            name="specification_title",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="specifications",
                to="products.specificationtitle",
            ),
        ),
    ]
