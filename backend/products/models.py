from django.core import validators
from django.db import models
from products import constants


class BaseProductModel(models.Model):
    title = models.CharField(max_length=250)
    short_title = models.CharField(max_length=50)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ("updated_at",)


class Product(BaseProductModel):
    quantity = models.PositiveIntegerField(default=1)
    selling_price = models.PositiveIntegerField()
    original_price = models.PositiveIntegerField(verbose_name="MRP", default=0)
    shipping_fee = models.PositiveIntegerField(default=0)
    category = models.CharField(max_length=50, choices=constants.CATEGORY)
    slogan = models.CharField(max_length=50, default="Best Selling")
    description = models.TextField(default="Product Description")

    def __str__(self):
        return f"{self.id} __ {self.title}"


class Discount(BaseProductModel):
    discount_percentage = models.PositiveIntegerField(validators=[validators.MaxValueValidator(100)])
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="discounts")

    def __str__(self):
        return f"{self.id} __ {self.title}"


class SpecificationTitle(BaseProductModel):
    short_title = None
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="specification_titles")

    class Meta:
        verbose_name = "Specification"
        verbose_name_plural = "Specifications"

    def __str__(self):
        return f"{self.id} __ {self.title}"


class Specification(models.Model):
    name = models.CharField(max_length=50, help_text="Example brand, weight, etc")
    value = models.CharField(max_length=150, help_text="Corresponding value of name")
    specification_title = models.ForeignKey(SpecificationTitle, on_delete=models.CASCADE, related_name="specifications")

    def __str__(self):
        return f"{self.id} __ {self.name}"


def product_image_directory(_, file_name):
    return f"product_images/{ProductImage.objects.count() + 1}__{file_name}"


class ProductImage(models.Model):
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name="images",
    )
    image_url = models.ImageField("Upload Image", upload_to=product_image_directory)

    def __str__(self):
        return str(self.image_url)
