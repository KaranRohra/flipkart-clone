import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./components/header/Header";
import Home from "./components/home/Home";
import Cart from "./components/cart/Cart";
import Footer from "./components/footer/Footer";
import PageNotFound from "./components/page-not-found/PageNotFound";
import TemplateProvider from "./providers/TemplateProvider";
import DataProvider from "./providers/DataProvider";
import Product from "./components/products/Product";

function App() {
    return (
        <DataProvider>
            <TemplateProvider>
                <Router>
                    <Header />
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/cart" component={Cart} />
                        <Route exact path="/products/:id" component={Product} />
                        <Route path="/" component={PageNotFound} />
                    </Switch>
                    <Footer />
                </Router>
            </TemplateProvider>
        </DataProvider>
    );
}

export default App;
