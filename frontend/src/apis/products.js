import baseTemplate from "./baseTemplate";

export const getHomePageProducts = async () => {
    const response = await baseTemplate({ method: "GET", url: "products/home" });
    return response.data;
};

export const getProductById = async (product_id) => {
    try {
        const response = await baseTemplate({
            method: "GET",
            url: `products/${product_id}`,
        });
        return response.data;
    } catch (err) {
        return false;
    }
};
