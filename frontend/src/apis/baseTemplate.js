import axios from "axios";

async function baseTemplate({ method, url, data, headers }) {
    // TODO await new Promise(resolve => setTimeout(resolve, 5000))

    return await axios({
        method: method,
        url: `${process.env.REACT_APP_BACKEND_URL}/${url}`,
        data: data,
        headers: headers,
    });
}

export default baseTemplate;
