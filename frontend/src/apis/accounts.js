import baseTemplate from "./baseTemplate";

export const verifyUser = async (data) => {
    try {
        const response = await baseTemplate({
            method: "POST",
            url: "accounts/login",
            data: data,
        });
        window.localStorage.setItem("token", `Token ${response.data.token}`);
        return getUserDetails();
    } catch (err) {
        return false;
    }
};

export const registerUser = async (data) => {
    try {
        await baseTemplate({
            method: "POST",
            url: "accounts/",
            data: data,
        });
        return await verifyUser({
            username: data.email,
            password: data.password,
        });
    } catch (err) {
        return false;
    }
};

export const getUserDetails = async () => {
    try {
        const response = await baseTemplate({
            method: "GET",
            url: "accounts/user",
            headers: {
                Authorization: window.localStorage.getItem("token"),
            },
        });
        return response.data;
    } catch (err) {
        return false;
    }
};

export const logoutUser = () => window.localStorage.clear();
