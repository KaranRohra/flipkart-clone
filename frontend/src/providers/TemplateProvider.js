import React from "react";
import { CssBaseline } from "@material-ui/core";
import { createTheme, ThemeProvider } from "@material-ui/core";

const TemplateContext = React.createContext(null);

function TemplateProvider(props) {
    const theme = createTheme({
        overrides: {
            MuiDialog: {
                paperWidthSm: {
                    maxWidth: "unset",
                },
            },
            MuiDialogContent: {
                root: {
                    padding: 0,
                    "&:first-child": {
                        paddingTop: 0,
                    },
                },
            },
        },
    });

    return (
        <TemplateContext.Provider value="">
            <ThemeProvider theme={theme}>
                <CssBaseline />
                {props.children}
            </ThemeProvider>
        </TemplateContext.Provider>
    );
}

export default TemplateProvider;
