import React from "react";
import { Dialog, DialogContent, CircularProgress } from "@material-ui/core";
import { getUserDetails } from "../apis/accounts";

export const DataContext = React.createContext();

function DataProvider(props) {
    const [userDetails, setUserDetails] = React.useState(null);
    const [productCount, setProductCount] = React.useState([]);
    const [loading, setLoading] = React.useState(true);

    React.useEffect(() => {
        const getContextData = async () => {
            setUserDetails(await getUserDetails());
            setLoading(false);
        };
        getContextData();
    }, []);

    const contextValue = {
        userDetails: userDetails,
        setUserDetails: setUserDetails,
        productCount: productCount,
        setProductCount: setProductCount,
    };

    return (
        <>
            {loading ? (
                <Dialog open={loading}>
                    <DialogContent>
                        {" "}
                        <CircularProgress />{" "}
                    </DialogContent>
                </Dialog>
            ) : (
                <DataContext.Provider value={contextValue}>{props.children}</DataContext.Provider>
            )}
        </>
    );
}

export default DataProvider;
