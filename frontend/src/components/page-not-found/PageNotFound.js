import React from "react";
import { Box, makeStyles, Typography, Button, Grid } from "@material-ui/core";
import { headerColor } from "../../constants/colors";
import { useHistory } from "react-router-dom";
import { pageNotFoundImage } from "../../constants/data";

const useStyles = makeStyles((theme) => ({
    container: {
        margin: theme.spacing(0, 10, 10, 10),
        padding: theme.spacing(10),
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        backgroundColor: null,
        "& > *": {
            padding: theme.spacing(1),
        },
    },
    homeButton: {
        backgroundColor: headerColor,
        color: "white",
    },
}));

function PageNotFound() {
    const classes = useStyles();
    const history = useHistory();

    return (
        <Grid container justifyContent="center">
            <Box className={classes.container}>
                <img src={pageNotFoundImage} alt="PageNotFound" />
                <Typography variant="h6">
                    Unfortunately the page you are looking for has been moved or deleted
                </Typography>
                <Button onClick={() => history.push("/")} variant="contained" className={classes.homeButton}>
                    Go To HomePage
                </Button>
            </Box>
        </Grid>
    );
}

export default PageNotFound;
