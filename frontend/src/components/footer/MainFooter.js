import React from "react";
import { makeStyles, Box, Grid, Typography, Divider, Link as MUILink } from "@material-ui/core";
import { mainFooterData, footerCompanyData } from "../../constants/data";
import { headerColor, footerTextHeadingColor } from "../../constants/colors";

const useStyles = makeStyles((theme) => ({
    footer: {
        padding: theme.spacing(6),
        display: "flex",
        justifyContent: "space-between",
    },
    divider: {
        background: "white",
    },
    footerItem: {
        "& > *": {
            fontSize: 12,
            padding: 2,
        },
    },
}));

function MainFooter() {
    const classes = useStyles();

    return (
        <Box className={classes.footer}>
            {Object.keys(mainFooterData).map((title, i) => (
                <Grid container direction="column" alignItems="flex-start" key={i} className={classes.footerItem}>
                    <Typography style={{ color: footerTextHeadingColor }}>{title}</Typography>
                    {mainFooterData[title].map((data, j) => (
                        <Typography style={{ color: "white" }} key={j}>
                            {data}
                        </Typography>
                    ))}
                </Grid>
            ))}

            <Divider style={{ marginRight: 20 }} orientation="vertical" classes={{ root: classes.divider }} flexItem />

            {Object.keys(footerCompanyData).map((item, index) => (
                <Grid container direction="column" alignItems="flex-start" key={index} className={classes.footerItem}>
                    <Typography style={{ color: footerTextHeadingColor }}>{item}</Typography>
                    <Typography style={{ color: "white" }}>
                        {footerCompanyData[item]}
                        {index === 1 && <MUILink style={{ color: headerColor }}>1800 202 9898</MUILink>}
                    </Typography>
                </Grid>
            ))}
        </Box>
    );
}

export default React.memo(MainFooter);
