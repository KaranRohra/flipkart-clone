import React from "react";
import { bottomFooterData } from "../../constants/data";
import { Grid, withStyles, Typography } from "@material-ui/core";

const BodyTypography = withStyles({
    root: {
        color: "white",
        fontSize: 12,
    },
})(Typography);

const BottomNavGrid = withStyles((theme) => ({
    root: {
        padding: theme.spacing(4, 7),
        "& > div": {
            display: "flex",
            "& > img": {
                marginRight: 5,
            },
        },
    },
}))(Grid);

function BottomFooter() {
    return (
        <BottomNavGrid container alignItems="center" justifyContent="space-between">
            {bottomFooterData.map((item, index) => (
                <div key={index}>
                    <img src={item.url} alt="" />
                    <BodyTypography>{item.text}</BodyTypography>
                </div>
            ))}
        </BottomNavGrid>
    );
}

export default React.memo(BottomFooter);
