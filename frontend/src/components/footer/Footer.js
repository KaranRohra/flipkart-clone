import React from "react";
import { makeStyles, Box, Divider } from "@material-ui/core";
import MainFooter from "./MainFooter";
import BottomFooter from "./BottomFooter";
import { footerColor } from "../../constants/colors";

const useStyles = makeStyles((theme) => ({
    footer: {
        backgroundColor: footerColor,
        marginTop: theme.spacing(5),
    },
    divider: {
        background: "white",
    },
}));

function Footer() {
    const classes = useStyles();

    return (
        <Box className={classes.footer}>
            <MainFooter />
            <Divider classes={{ root: classes.divider }} />
            <BottomFooter />
        </Box>
    );
}

export default React.memo(Footer);
