import React from "react";
import { DialogContent, Dialog, makeStyles } from "@material-ui/core";
import Form from "./Form";
import { registerUser, verifyUser } from "../../apis/accounts";

const useStyles = makeStyles({
    dialogContainer: {
        height: "80vh",
        width: "90vh",
    },
});

function Auth(props) {
    const classes = useStyles();
    const [formToggle, setFormToggle] = React.useState(true);
    const [loading, setLoading] = React.useState(false);
    const [error, setError] = React.useState(null);

    const handleAutData = async ({ data, handlerFunction, errorMessage }) => {
        setLoading(true);
        const response = await handlerFunction(data);
        setLoading(false);

        if (response) {
            props.setUserDetails(response);
            props.setOpenForm(false);
            setError(null);
        } else {
            setError(errorMessage);
        }
    };

    const handleRegisterData = (data) => {
        handleAutData({
            data: data,
            handlerFunction: registerUser,
            errorMessage: "Account with this email is already exist",
        });
    };

    const handleLoginData = async (data) => {
        handleAutData({
            data: data,
            handlerFunction: verifyUser,
            errorMessage: "Your email or password is incorrect",
        });
    };

    const handleFormClose = () => {
        props.setOpenForm(false);
        setFormToggle(true);
        setError(null);
    };

    const handleFormToggle = () => {
        setFormToggle(!formToggle);
        setError(null);
    };

    return (
        <Dialog open={props.openForm} onClose={handleFormClose}>
            <DialogContent className={classes.dialogContainer}>
                {formToggle ? (
                    <Form
                        imageTitle="Login"
                        imageText="Get access to your Orders, Wishlist and Recommendations"
                        type="login"
                        handleFormToggle={handleFormToggle}
                        open={props.openForm}
                        setOpen={props.setOpenForm}
                        submitAction={handleLoginData}
                        error={error}
                        loading={loading}
                    />
                ) : (
                    <Form
                        imageTitle="Looks like you're new here!"
                        imageText="Get access to your Orders, Wishlist and Recommendations"
                        type="register"
                        handleFormToggle={handleFormToggle}
                        open={props.openForm}
                        setOpen={props.setOpenForm}
                        submitAction={handleRegisterData}
                        error={error}
                        loading={loading}
                    />
                )}
            </DialogContent>
        </Dialog>
    );
}

export default Auth;
