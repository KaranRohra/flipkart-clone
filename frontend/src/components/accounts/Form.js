import React from "react";
import {
    makeStyles,
    Box,
    Typography,
    TextField,
    Button,
    Checkbox,
    FormControlLabel,
    CircularProgress,
} from "@material-ui/core";
import { authImage } from "../../constants/data";
import { footerTextHeadingColor, headerColor, submitButtonColor } from "../../constants/colors";
import { Alert } from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
    image: {
        backgroundImage: `url(${authImage})`,
        height: "80vh",
        backgroundRepeat: "no-repeat",
        background: headerColor,
        width: "40%",
        backgroundPosition: "center 85%",
        "& > p": {
            color: "white",
        },
    },
    loginText: {
        fontWeight: 600,
        padding: theme.spacing(5, 5, 1, 5),
        fontSize: 30,
    },
    imageText: {
        padding: theme.spacing(1, 5, 2, 5),
        fontSize: 18,
    },
    authForm: {
        padding: theme.spacing(2, 5),
        display: "flex",
        flex: 1,
        flexDirection: "column",
        "& > *": {
            marginBottom: "20px",
        },
    },
    submitBtn: {
        textTransform: "unset",
        background: submitButtonColor,
        color: "white",
        fontWeight: 600,
        height: "45px",
    },
    requestOtp: {
        textTransform: "unset",
        background: "white",
        color: headerColor,
        fontWeight: 600,
        height: "45px",
    },
    text: {
        color: footerTextHeadingColor,
        fontSize: 12,
    },
    linkText: {
        textAlign: "center",
        marginTop: "auto",
        color: headerColor,
        fontWeight: 600,
        cursor: "pointer",
        fontSize: 14,
    },
    forgotPassword: {
        color: headerColor,
        fontSize: 14,
        cursor: "pointer",
    },
    container: {
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "auto",
        marginBottom: "auto",
    },
}));

function Form(props) {
    const classes = useStyles();
    const [showPassword, setShowPassword] = React.useState(false);
    const [formData, setFormData] = React.useState({});

    function handleForm(event) {
        event.preventDefault();
        props.submitAction(formData);
    }

    return (
        <Box style={{ display: "flex" }}>
            <Box className={classes.image}>
                <Typography className={classes.loginText}>{props.imageTitle}</Typography>
                <Typography className={classes.imageText}>{props.imageText}</Typography>
            </Box>
            <Box className={classes.container}>
                {props.loading ? (
                    <CircularProgress />
                ) : (
                    <form onSubmit={handleForm} className={classes.authForm} method="POST">
                        {props.error && <Alert severity="error">{props.error}</Alert>}
                        {props.type === "register" && (
                            <>
                                <TextField
                                    name="firstName"
                                    label="First Name"
                                    required
                                    onChange={(e) => setFormData({ ...formData, first_name: e.target.value })}
                                />
                                <TextField
                                    name="lastName"
                                    label="Last Name"
                                    required
                                    onChange={(e) => setFormData({ ...formData, last_name: e.target.value })}
                                />
                            </>
                        )}
                        <TextField
                            type="email"
                            name="email"
                            label="Email"
                            required
                            onChange={(e) =>
                                setFormData({ ...formData, username: e.target.value, email: e.target.value })
                            }
                        />
                        <TextField
                            name="password"
                            label="Password"
                            type={showPassword ? "text" : "password"}
                            required
                            onChange={(e) => setFormData({ ...formData, password: e.target.value })}
                        />
                        <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        color="primary"
                                        checked={showPassword}
                                        onChange={() => setShowPassword(!showPassword)}
                                    />
                                }
                                label="Show password"
                            />
                            {props.type === "login" && (
                                <Typography className={classes.forgotPassword}>Forgot Password?</Typography>
                            )}
                        </div>
                        <Typography className={classes.text}>
                            By continuing, you agree to Flipkart's Terms of Use and Privacy Policy.
                        </Typography>
                        <Button variant="contained" className={classes.submitBtn} type="submit">
                            {props.type === "login" ? "Login" : "Register"}
                        </Button>
                        {props.type === "login" ? (
                            <>
                                <Typography className={classes.text} style={{ textAlign: "center" }}>
                                    OR
                                </Typography>
                                <Button variant="contained" className={classes.requestOtp}>
                                    Request OTP
                                </Button>
                                <Typography className={classes.linkText} onClick={props.handleFormToggle}>
                                    New to Flipkart? Create an account
                                </Typography>
                            </>
                        ) : (
                            <Button variant="contained" className={classes.requestOtp} onClick={props.handleFormToggle}>
                                Existing User? Log in
                            </Button>
                        )}
                    </form>
                )}
            </Box>
        </Box>
    );
}

export default React.memo(Form);
