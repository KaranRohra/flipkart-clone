import React from "react";
import { Box, CircularProgress, Dialog, DialogContent, makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { getProductById } from "../../apis/products";
import { productBackgroundColor } from "../../constants/colors";
import ProductDetails from "./ProductDetails";
import ProductImages from "./ProductImages";

const useStyles = makeStyles((theme) => ({
    wrapper: {
        backgroundColor: productBackgroundColor,
    },
    container: {
        margin: theme.spacing(0, 10),
        background: "white",
        display: "flex",
    },
}));

function Product(props) {
    const classes = useStyles();
    const history = useHistory();
    const [productDetails, setProductDetails] = React.useState(null);

    const productId = props.match.params.id;

    React.useEffect(() => {
        const getProductDetails = async () => {
            const response = await getProductById(productId);
            if (response) {
                setProductDetails(response);
            } else {
                history.push("/");
            }
        };
        getProductDetails();
    }, [productId, history]);

    return (
        <>
            {productDetails ? (
                <Box className={classes.wrapper}>
                    <Box className={classes.container}>
                        {/* Left Container */}
                        <ProductImages productDetails={productDetails} />
                        {/* Right Container */}
                        <ProductDetails productDetails={productDetails} />
                    </Box>
                </Box>
            ) : (
                <Dialog open={true}>
                    <DialogContent>
                        <CircularProgress />
                    </DialogContent>
                </Dialog>
            )}
        </>
    );
}

export default Product;
