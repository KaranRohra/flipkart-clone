import React from "react";
import { makeStyles, Box, Button } from "@material-ui/core";
import { headerColor, lightBorderColor } from "../../constants/colors";

const useStyles = makeStyles({
    specificationDetails: {
        marginTop: "24px",
        borderRadius: "2px",
        fontSize: "12px",
        border: `1px solid ${lightBorderColor}`,
    },
    specificationText: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        padding: " 24px 30px 24px 24px",
        fontSize: 24,
        fontWeight: 600,
        lineHeight: 1.14,
        color: "#212121",
    },
    specification: {
        border: `1px solid ${lightBorderColor}`,
        padding: "24px 24px 34px",
        fontSize: "14px",
    },
    specificationTitle: {
        paddingBottom: "16px",
        fontSize: "18px",
        whiteSpace: "nowrap",
        lineHeight: 1.4,
        textWrap: "none",
    },
    specificationTable: {
        width: "100%",
        borderCollapse: "collapse",
    },
    specificationRow: {
        paddingBottom: "16px",
        display: "flex",
    },
    specificationName: {
        width: "25%",
        color: "#878787",
        paddingRight: "8px",
    },
    specificationValue: {
        width: "75%",
        lineHeight: 1.4,
        wordBreak: "break-word",
        color: "#212121",
    },
    readMore: {
        fontSize: 18,
        width: "100%",
        color: headerColor,
        textTransform: "none",
        textAlign: "left",
        border: `1px solid ${lightBorderColor}`,
    },
});

function Specification(props) {
    const [readMore, setReadMore] = React.useState(false);
    const classes = useStyles();

    return (
        <Box className={classes.specificationDetails}>
            {/* Specifications */}
            <Box className={classes.specificationText}>Specification</Box>
            {/* Specifications Details */}
            <Box>
                {props.specificationTitles.map((details, i) => (
                    <Box
                        style={{ display: i === 0 || readMore ? "block" : "none" }}
                        className={classes.specification}
                        key={i}
                    >
                        <Box className={classes.specificationTitle}>{details.title}</Box>
                        <table className={classes.specificationTable}>
                            <tbody>
                                {details.specifications.map((specification, j) => (
                                    <tr className={classes.specificationRow} key={j}>
                                        <td className={classes.specificationName}>{specification.name}</td>
                                        <td className={classes.specificationValue}>{specification.value}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </Box>
                ))}
                {!readMore && (
                    <Button onClick={() => setReadMore(true)} className={classes.readMore}>
                        Read More
                    </Button>
                )}
            </Box>
        </Box>
    );
}

export default Specification;
