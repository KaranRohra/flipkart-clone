import React from "react";
import { Box, makeStyles, Typography } from "@material-ui/core";
import { footerTextHeadingColor, headerColor, productBackgroundColor } from "../../constants/colors";
import clsx from "clsx";
import { flipkartPlus, offerBadgeImage, ratingStarImage } from "../../constants/data";
import { formatToCurrency } from "../../utils/helper";
import Specification from "./Specification";

const useStyles = makeStyles((theme) => ({
    wrapper: {
        backgroundColor: productBackgroundColor,
    },
    container: {
        margin: theme.spacing(0, 10),
        background: "white",
        display: "flex",
    },
    detailContainer: {
        marginTop: 20,
        "& > *": {
            marginBottom: 5,
        },
    },
    smallText: {
        fontSize: 14,
        "& > *": {
            fontSize: 14,
            marginTop: 5,
        },
    },
    mediumText: {
        fontSize: 18,
        fontWeight: 600,
    },
    largeText: {
        fontSize: 28,
        fontWeight: 600,
    },
    greyText: {
        color: footerTextHeadingColor,
    },
    offerBadge: {
        height: 18,
        marginRight: 10,
    },
    ratingStars: {
        fontSize: 12,
        fontWeight: 500,
        borderRadius: 3,
        verticalAlign: "middle",
        lineHeight: "normal",
        background: "#388e3c;",
        color: "white",
        display: "inline-block",
        padding: "2px 4px 2px 6px;",
        marginRight: 10,
    },
    startImage: {
        height: 10,
        margin: "2px 0 0 2px",
    },
    readMore: {
        color: headerColor,
        cursor: "pointer",
    },
}));

function ProductDetails({ productDetails }) {
    const classes = useStyles();

    return (
        <Box className={classes.detailContainer}>
            <Typography>{productDetails.title}</Typography>
            <Box style={{ display: "flex", marginBottom: 10 }}>
                <Box className={classes.ratingStars}>
                    4.8
                    <img className={classes.startImage} src={ratingStarImage} alt="" />
                </Box>
                <Typography className={clsx(classes.smallText, classes.greyText)}>8 Ratings and 1 Review</Typography>
            </Box>
            <Typography className={classes.smallText} style={{ color: "green" }}>
                Extra ₹2000 off
            </Typography>
            <Box>
                <span className={classes.largeText}>₹{formatToCurrency(productDetails.selling_price)}</span>
                &nbsp;&nbsp;
                <span className={clsx(classes.greyText, classes.mediumText)}>
                    <strike>₹{formatToCurrency(productDetails.original_price)}</strike>
                </span>
                &nbsp;&nbsp;
                <span className={classes.mediumText} style={{ color: "green" }}>
                    5%off
                </span>
            </Box>
            <Box className={classes.smallText}>
                <Typography style={{ fontWeight: "bold" }}>Available Offers</Typography>
                <Typography>
                    <img className={classes.offerBadge} src={offerBadgeImage} alt="" />
                    <span style={{ fontWeight: "bold" }}> Bank Offer</span> 5% Unlimited Cashback on Flipkart Axis Bank
                    Credit Card
                </Typography>

                <Typography>
                    <img className={classes.offerBadge} src={offerBadgeImage} alt="" />
                    <span style={{ fontWeight: "bold" }}> Bank Offer</span> 20% off on 1st txn with Amex Network Cards
                    issued by ICICI Bank and IndusInd Bank
                </Typography>

                <Typography>
                    <img className={classes.offerBadge} src={offerBadgeImage} alt="" />
                    <span style={{ fontWeight: "bold" }}> Bank Offer</span> 10% Off on Bank of Baroda Mastercard debit
                    card first time transaction, Terms and Condition apply
                </Typography>
            </Box>
            {/* TODO add delivery and description section */}
            <img src={flipkartPlus} alt="" style={{ width: 400, marginTop: 20 }} />

            {/* Specification */}
            <Specification specificationTitles={productDetails.specification_titles} />
        </Box>
    );
}

export default ProductDetails;
