import React from "react";
import { Box, Button, makeStyles } from "@material-ui/core";
import Carousel from "react-material-ui-carousel";
import { ShoppingCart, FlashOn } from "@material-ui/icons";
import { DataContext } from "../../providers/DataProvider";

const useStyles = makeStyles((theme) => ({
    productImageWrapper: {
        margin: 50,
        width: 400,
        height: 400,
        "& > img": {
            height: "100%",
            width: "100%",
            objectFit: "contain",
        },
    },
    imageContainer: {
        minWidth: "40%",
        height: 700,
        position: "sticky",
    },
    buttonWrapper: {
        padding: 30,
    },
    addToCart: {
        width: "45%",
        fontWeight: "bold",
        color: "white",
        float: "left",
        background: "#ff9f00;",
        padding: "18px 8px",
    },
    buyNow: {
        width: "45%",
        fontWeight: "bold",
        padding: "18px 8px",
        float: "right",
        background: "#fb641b;",
        color: "white",
    },
}));

function ProductImages({ productDetails }) {
    const classes = useStyles();
    const dataContext = React.useContext(DataContext);

    const handleProductCount = () => {
        dataContext.setProductCount(new Set([...dataContext.productCount, productDetails.id]));
    };

    return (
        <Box className={classes.imageContainer}>
            <Carousel navButtonsAlwaysVisible stopAutoPlayOnHover>
                {productDetails.images.map((image, index) => (
                    <Box className={classes.productImageWrapper} key={index}>
                        <img src={image.image_url} alt="" />
                    </Box>
                ))}
            </Carousel>
            <Box className={classes.buttonWrapper}>
                <Button
                    startIcon={<ShoppingCart />}
                    onClick={handleProductCount}
                    variant="contained"
                    className={classes.addToCart}
                >
                    Add to Cart
                </Button>
                <Button
                    startIcon={<FlashOn />}
                    // TODO onClick={handleProductCount}
                    variant="contained"
                    className={classes.buyNow}
                >
                    Buy Now
                </Button>
            </Box>
        </Box>
    );
}

export default ProductImages;
