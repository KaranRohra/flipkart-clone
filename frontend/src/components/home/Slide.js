import React from "react";
import { makeStyles, Box, Typography, Button, Divider } from "@material-ui/core";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { timerURL } from "../../constants/data";
import Countdown from "react-countdown";
import { headerColor } from "../../constants/colors";
import { useHistory } from "react-router-dom";

const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 5,
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
    },
};

const useStyles = makeStyles((theme) => ({
    image: {
        height: "100%",
        width: " 100%",
        objectFit: "contain",
    },
    wrapper: {
        display: "inline-block",
        height: "150px",
        width: "250px",
        textAlign: "center",
    },
    container: {
        margin: theme.spacing(2, 0, 2, 0),
    },
    deal: {
        padding: theme.spacing(0.5, 1),
        display: "flex",
    },
    slideTitle: {
        fontSize: 22,
        fontWeight: 600,
        lineHeight: "32px",
        marginRight: 25,
    },
    timer: {
        marginLeft: 10,
        display: "flex",
        alignItems: "center",
    },
    viewAll: {
        marginLeft: "auto",
        background: headerColor,
        textTransform: "none",
        color: "white",
    },
    imageText: {
        fontSize: 14,
        marginTop: 5,
    },
}));

function Slide(props) {
    const classes = useStyles();
    const history = useHistory();

    const renderer = ({ hours, minutes, seconds }) => {
        return (
            <span className={classes.timer}>
                {hours}: {minutes}: {seconds} Left
            </span>
        );
    };

    function handleProductClick(product_id) {
        history.push(`/products/${product_id}`);
    }

    return (
        <Box className={classes.container}>
            <Box className={classes.deal} m={1}>
                <Typography className={classes.slideTitle}>{props.title}</Typography>
                {props.timer && (
                    <>
                        <img src={timerURL} alt="timer" style={{ width: 24, paddingLeft: 5 }} />
                        <Countdown date={Date.now() + 5.04e7} renderer={renderer} />
                    </>
                )}
                <Button className={classes.viewAll} variant="contained">
                    View all
                </Button>
            </Box>
            <Divider style={{ marginBottom: "15px" }} />
            <Carousel
                dotListClass="custom-dot-list-style"
                itemClass="carousel-item-padding-40-px"
                responsive={responsive}
                infinite={true}
                centerMode={true}
                draggable={false}
                swipeable={false}
                customTransition="all .5"
                autoPlay
                autoPlaySpeed={10000}
            >
                {props.products.map((product, index) => (
                    <Box key={index} onClick={() => handleProductClick(product.id)} className={classes.wrapper}>
                        <img src={product.image_url} alt="productImage" className={classes.image} />
                        <Typography className={classes.imageText} style={{ fontWeight: 600 }}>
                            {product.title}
                        </Typography>
                        <Typography className={classes.imageText} style={{ color: "green" }}>
                            {product.discount_title}
                        </Typography>
                        <Typography className={classes.imageText} style={{ opacity: 0.6 }}>
                            {product.slogan}
                        </Typography>
                    </Box>
                ))}
            </Carousel>
        </Box>
    );
}

export default Slide;
