import React from "react";
import { coronaURL, sectionImageUrl } from "../../constants/data";
import { Box, makeStyles, CircularProgress } from "@material-ui/core";
import Slide from "./Slide";
import { getHomePageProducts } from "../../apis/products";

const useStyles = makeStyles({
    section1Image: {
        width: "33%",
    },
    section1: {
        display: "flex",
        justifyContent: "space-between",
    },
    loader: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginTop: "50px",
    },
});

function Section() {
    const classes = useStyles();
    const [products, setProducts] = React.useState([]);
    const [loading, setLoading] = React.useState(true);

    React.useEffect(() => {
        const getProducts = async () => {
            const response = await getHomePageProducts();
            setProducts(response);
            setLoading(false);
        };
        getProducts();
    }, []);

    return (
        <>
            {loading ? (
                <Box className={classes.loader}>
                    <CircularProgress />
                </Box>
            ) : (
                <Box>
                    <Slide products={products.slice(0, 8)} title="Deal of the Day" timer />

                    <Box className={classes.section1}>
                        {sectionImageUrl.map((image, index) => (
                            <img key={index} src={image} alt="" className={classes.section1Image} />
                        ))}
                    </Box>
                    <Slide products={products.slice(8, 16)} title="Best Price on Electronics" />
                    <a href="https://pmnrf.gov.in/en/online-donation" target="_blank" rel="noreferrer">
                        <img src={coronaURL} alt="" style={{ width: "100%" }} />
                    </a>
                    <Slide products={products.slice(10, 16)} title="Recently viewed" />
                </Box>
            )}
        </>
    );
}

export default React.memo(Section);
