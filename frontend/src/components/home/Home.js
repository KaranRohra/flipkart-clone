import React from "react";
import Banner from "./Banner";
import NavBar from "./NavBar";
import Section from "./Section";

function Home() {
    return (
        <div style={{ margin: 5 }}>
            <NavBar />
            <Banner />
            <Section />
        </div>
    );
}

export default React.memo(Home);
