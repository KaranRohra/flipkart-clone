import React from "react";
import { Grid, Typography, makeStyles } from "@material-ui/core";
import { Link } from "react-router-dom";
import { navData } from "../../constants/data";

const useStyles = makeStyles((theme) => ({
    nav: {
        textAlign: "center",
        padding: theme.spacing(1),
    },
    navItemImage: {
        width: 60,
    },
    navItem: {
        textDecoration: "none",
        color: "black",
        "& > p": {
            fontSize: "14px",
        },
    },
}));

function NavBar() {
    const classes = useStyles();
    return (
        <Grid container justifyContent="space-evenly" className={classes.nav}>
            {navData.map((navItem, index) => (
                <Grid item key={index}>
                    <Link className={classes.navItem} to="/">
                        <img src={navItem.url} alt={navItem.text} className={classes.navItemImage} />
                        <Typography> {navItem.text} </Typography>
                    </Link>
                </Grid>
            ))}
        </Grid>
    );
}

export default React.memo(NavBar);
