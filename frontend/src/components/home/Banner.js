import React from "react";
import Carousel from "react-material-ui-carousel";
import { makeStyles } from "@material-ui/styles";
import { bannerData } from "../../constants/data";
import { Link } from "react-router-dom";
import { Box } from "@material-ui/core";

const useStyles = makeStyles(() => ({
    bannerImage: {
        width: "100%",
    },
    banner: {
        backgroundColor: "#edf2ef",
    },
}));

function Banner() {
    const classes = useStyles();

    return (
        <Box p={1} className={classes.banner}>
            <Carousel
                autoPlay={true}
                animation="slide"
                indicators={false}
                navButtonsAlwaysVisible={true}
                navButtonsProps={{
                    style: {
                        backgroundColor: "white",
                        color: "black",
                        borderRadius: 0,
                    },
                }}
            >
                {bannerData.map((bannerImage, index) => (
                    <Link to="/" key={index}>
                        <img src={bannerImage} alt="bannerImage" className={classes.bannerImage} />
                    </Link>
                ))}
            </Carousel>
        </Box>
    );
}

export default React.memo(Banner);
