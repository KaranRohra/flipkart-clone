import React from "react";
import { Grid, Button, makeStyles, Box, Badge } from "@material-ui/core";
import { ShoppingCart } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import { headerColor } from "../../constants/colors";
import Auth from "../accounts/Auth";
import { DataContext } from "../../providers/DataProvider";
import Profile from "./Profile";

const useStyles = makeStyles({
    login: {
        textTransform: "none",
        width: 120,
        fontSize: 14,
        fontWeight: 700,
        backgroundColor: "white",
        color: headerColor,
    },
    cart: {
        textTransform: "none",
        fontSize: 18,
        fontWeight: 700,
        color: "white",
    },
    container: {
        width: "50%",
    },
});

function HeaderButtons() {
    const classes = useStyles();
    const history = useHistory();
    const [openForm, setOpenForm] = React.useState(false);
    const dataContext = React.useContext(DataContext);

    return (
        <Box className={classes.container}>
            <Grid container justifyContent="center" alignItems="center" spacing={7}>
                <Grid item>
                    {dataContext.userDetails ? (
                        <Profile />
                    ) : (
                        <Button variant="contained" className={classes.login} onClick={() => setOpenForm(!openForm)}>
                            Login
                        </Button>
                    )}
                </Grid>
                <Grid item>
                    <Button
                        startIcon={
                            <Badge badgeContent={dataContext.productCount.size} color="secondary">
                                <ShoppingCart />
                            </Badge>
                        }
                        className={classes.cart}
                        onClick={() => history.push("/cart")}
                    >
                        Cart
                    </Button>
                </Grid>
            </Grid>
            <Auth openForm={openForm} setOpenForm={setOpenForm} setUserDetails={dataContext.setUserDetails} />
        </Box>
    );
}

export default React.memo(HeaderButtons);
