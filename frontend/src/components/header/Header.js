import React from "react";
import { AppBar, Toolbar, makeStyles, withStyles } from "@material-ui/core";
import { Link } from "react-router-dom";
import SearchBar from "./SearchBar";
import { headerLogoURL } from "../../constants/data";
import { headerColor } from "../../constants/colors";
import HeaderButtons from "./HeaderButtons";

const useStyles = makeStyles((theme) => ({
    header: {
        backgroundColor: headerColor,
    },
    headerLogo: {
        [theme.breakpoints.up("md")]: {
            marginLeft: 140,
        },
    },
}));

const MyToolbar = withStyles({
    root: {
        minHeight: 55,
    },
})(Toolbar);

function Header() {
    const classes = useStyles();

    return (
        <div>
            <AppBar className={classes.header}>
                <MyToolbar>
                    <Link to="/" className={classes.headerLogo}>
                        <img style={{ width: 100 }} src={headerLogoURL} alt="Flipkart" />
                    </Link>
                    <SearchBar />
                    <HeaderButtons />
                </MyToolbar>
            </AppBar>
            <MyToolbar />
        </div>
    );
}

export default Header;
