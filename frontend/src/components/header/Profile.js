import React from "react";
import { Button, Menu, MenuItem, makeStyles } from "@material-ui/core";
import { AccountCircle, PowerSettingsNew, Favorite, Input } from "@material-ui/icons";
import { headerColor } from "../../constants/colors";
import { DataContext } from "../../providers/DataProvider";
import { logoutUser } from "../../apis/accounts";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles({
    menu: {
        marginTop: 40,
    },
    menuItem: {
        height: 30,
        width: 200,
        "& > *": {
            padding: 4,
        },
    },
    profileButton: {
        color: "white",
        textTransform: "none",
        fontWeight: 600,
        fontSize: 18,
        cursor: "pointer",
    },
});

function Profile() {
    const classes = useStyles();
    const history = useHistory();
    const [open, setOpen] = React.useState(false);
    const dataContext = React.useContext(DataContext);

    const handleClose = (url) => {
        if (url === "logout") {
            logoutUser();
            dataContext.setUserDetails(null);
            dataContext.setProductCount(0);
        } else {
            history.push(url);
        }
        setOpen(false);
    };

    const handleClick = (event) => setOpen(event.currentTarget);

    const menuItems = [
        { url: "accounts/profile", icon: <AccountCircle style={{ color: headerColor }} />, text: "My Profile" },
        { url: "orders", icon: <Input style={{ color: headerColor }} />, text: "Orders" },
        { url: "wishlist", icon: <Favorite style={{ color: headerColor }} />, text: "Wishlist" },
        { url: "logout", icon: <PowerSettingsNew style={{ color: headerColor }} />, text: "Logout" },
    ];

    return (
        <div>
            <Button className={classes.profileButton} onClick={handleClick}>
                {dataContext.userDetails.first_name}
            </Button>
            <Menu anchorEl={open} open={Boolean(open)} onClose={handleClose} className={classes.menu}>
                {menuItems.map((item, index) => (
                    <MenuItem className={classes.menuItem} onClick={() => handleClose(item.url)} key={index}>
                        {item.icon}
                        <p style={{ fontSize: 14 }}>{item.text}</p>
                    </MenuItem>
                ))}
            </Menu>
        </div>
    );
}

export default Profile;
