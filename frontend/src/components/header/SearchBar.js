import React from "react";
import { InputBase, makeStyles, Box, IconButton } from "@material-ui/core";
import { Search } from "@material-ui/icons";
import { headerColor } from "../../constants/colors";

const useStyles = makeStyles((theme) => ({
    search: {
        background: "white",
        marginLeft: 10,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        width: "40%",
        height: 35,
    },
    searchIcon: {
        color: headerColor,
    },
    inputRoot: {
        fontSize: 14,
        width: "100%",
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 2),
    },
}));

function SearchBar() {
    const classes = useStyles();

    return (
        <Box boxShadow={2} className={classes.search}>
            <InputBase
                placeholder="Search for products, brands and many more"
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                }}
                inputProps={{ "aria-label": "search" }}
            />
            <IconButton className={classes.searchIcon}>
                <Search />
            </IconButton>
        </Box>
    );
}

export default SearchBar;
