export const headerColor = "#2874f0";
export const footerColor = "#172337";
export const footerTextHeadingColor = "#878787";
export const submitButtonColor = "#fc7826";
export const productBackgroundColor = "#ebeef0";
export const lightBorderColor = "#f0f0f0";
